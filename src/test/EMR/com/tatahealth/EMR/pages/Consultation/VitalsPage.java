package com.tatahealth.EMR.pages.Consultation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class VitalsPage {
	
	public WebElement moveToVitalModule(WebDriver driver,ExtentTest  logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li[@data-input='consultvitals']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement saveVitals(WebDriver driver,ExtentTest logger)throws Exception{
		String xpath = "//ul[@id='fixedmenu']//li[@data-input='Print']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getJSErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='popover fade top in popover-danger'][contains(@style,'display: block;')]/div[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	//BP
	public WebElement getBPSysElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='txtBloodPressure']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getBPDiaElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='txtBloodPressure2']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getBPUnit(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='Blood_Pressure_Field_Container']//span[@class='small'][contains(text(),'(mmHg)')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	
	//spo2
	public WebElement getSpo2Element(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='txtSpO2']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSpo2Unit(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[contains(text(),'(%)')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	//temperature
	public WebElement getTemperatureElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='txtTemperature']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTemperatureUnit(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='Temperature_Field_Container']//span[@class='small'][contains(text(),'(F)')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	//pulse rate
	public WebElement getPulseRateElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='txtPulseRate']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getPulseRateUnit(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[contains(text(),'(BPM)')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	//weight
	public WebElement getWeightElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='txtWeight']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement getWeightUnitElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[contains(text(),'(kg)')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	//BMI
	public WebElement getBMIElement(WebDriver driver,ExtentTest logger)throws Exception{
			
		String xpath = "//input[@id='txtBMI']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	
	}
		
	public WebElement getBMIUnitElement(WebDriver driver,ExtentTest logger)throws Exception{
			
		String xpath = "//div[@id='BMI_Field_Container']/div[2]/span[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
			
	}
		
	//height
	public WebElement getHeightElement(WebDriver driver,ExtentTest logger)throws Exception{
			
		String xpath = "//input[@id='txtHeight']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
			
	}
		
	public WebElement getHeightUnitElement(WebDriver driver,ExtentTest logger)throws Exception{
			
		String xpath = "//span[contains(text(),'(cms)')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;			
	}
	
	//add new vital parameter
	public WebElement getAddNewVitalParameter(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='addNewVitalParameter']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	//vital notes
	public WebElement getVitalNotesElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//textarea[@id='txtparameterComment']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
		
	}
	
	//tooth eruption from add paramter
	public WebElement getToothEruptionParameter(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Tooth Eruption')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	public WebElement getToothEruptionLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[contains(text(),'Tooth Eruption')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}

	public WebElement getToothEruptionElement(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='txtToothEruption']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	

	public boolean isToothEruptionDisplayed(WebDriver driver,ExtentTest looger)throws Exception{
		
		String xpath = "//span[contains(text(),'Tooth Eruption')]";
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return status;
	}
	
	//hip girth
	public WebElement getHipGirthParameter(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='vitalParameterCheckbox_19']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	public boolean isHipGirthParameterChecked(WebDriver driver,ExtentTest logger)throws Exception{
	
		String xpath = "//input[@id='vitalParameterCheckbox_19']";
		boolean status = Web_GeneralFunctions.isChecked(xpath, driver);
		return status;	
	}
	
	public boolean isHipGirthDisplayed(WebDriver driver,ExtentTest looger)throws Exception{
		
		String xpath = "//div[@id='Hip_Girth_Field_Container']//span[@class='graytext'][contains(text(),'Hip Girth')]";
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return status;
	}
	
	
	public WebElement getHipGirthLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='Hip_Girth_Field_Container']//span[@class='graytext'][contains(text(),'Hip Girth')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	//add button from parameter form
	public WebElement getAddFromParameterForm(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='addFieldsToVitalForm']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	
	public WebElement getAddParameterClosePopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='vitalMasterConfigModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	public WebElement getNotesLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'topspacemore')]//label[contains(text(),'Notes')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
		
	}
	
	public WebElement getVitalsLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//h4[contains(text(),'Vitals')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getNonEmptyFieldMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Trying to delete non-empty field !!')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getVitalsSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Vitals details have been saved successfully')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTemperatureMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please enter a value between 0 and 115')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getPulseRateMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please enter a value between 0 and 300')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getSpo2Message(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'SpO2% value should be from 0 to 100')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public boolean isHeadCircumferenceDisplayed(WebDriver driver,ExtentTest looger)throws Exception{
		
		String xpath = "//span[contains(text(),'Head Circumference')]";
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return status;
	}
	
	public boolean isVitalsDisplayed(String value,WebDriver driver,ExtentTest looger)throws Exception{
		
		String xpath = "//span[contains(text(),'"+value+"')]";
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return status;
	}
		
}
