package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class GeneralAdviceTemplatePages {

	public WebElement moveToGeneralAdviceTemplateModule(WebDriver driver,ExtentTest logger){
		
		String xpath = "//a[contains(text(),'General Advice')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getGeneralAdviceHeader(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),'General Advice')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSaveButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@class='col-lg-6 text-right padtop15 center']//button[@class='btn btn-default takephotobtn'][contains(text(),'Save')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDeleteButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='deleteGeneralAdviceButton']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSelectGeneralAdviceTemplate(WebDriver driver,ExtentTest logger){
		
		String xpath = "//select[@id='generalAdviceTemplateType']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCreateTemplate(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='toggleTemplate']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getCreatetemplateName(WebDriver driver,ExtentTest logger){
		
		String xpath = "//input[@id='createTemplateName']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getGeneralAdviceTextArea(WebDriver driver,ExtentTest logger){
		
		String xpath = "//textarea[@id='generalAdviceText']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
		
	}
	
	public WebElement getGeneralAdviceTemplateSearch(WebDriver driver,ExtentTest logger){
		
		String xpath = "//input[@id='search']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
		
	}
	
	public WebElement getSelectTemplateButton(WebDriver driver,ExtentTest logger) {
		String xpath = "//button[contains(text(),'Select Template')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public WebElement getFirstGeneralAdviceTemplate(String templateName,WebDriver driver,ExtentTest logger) {
		String xpath = "//ul[@id='dropdown']/li/label[contains(text(),'"+templateName+"')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
	}
	
	public List<String> getAllGeneralAdviceTemplatesNamesAslist(WebDriver driver,ExtentTest logger) throws Exception {
		String xpath ="//select[@name='generalAdviceTemplateType']//option";
		List<String> GeneralAdviceTemplateNames = new ArrayList<>();
		List<WebElement> allTemplates = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		for(WebElement template : allTemplates) {
			GeneralAdviceTemplateNames.add(Web_GeneralFunctions.getText(template, "extracting templateName", driver, logger));
		}
		return GeneralAdviceTemplateNames;
	}
	
}
