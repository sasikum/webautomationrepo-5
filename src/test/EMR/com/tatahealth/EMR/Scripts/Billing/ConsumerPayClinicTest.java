package com.tatahealth.EMR.Scripts.Billing;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerPayClinicTest {
	ConsultationTest appt=new ConsultationTest();
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	ConsultationPage consultation = new ConsultationPage();
	ConsumerAppointmentTest consumer= new ConsumerAppointmentTest();
	public static ExtentTest logger;
	
	
	@BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="ConsumerPayClinicTest";
		  BillingTest.doctorName="Topaa";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
			Reports.extent.flush(); 
		  }
	  
	  
	  /*Billing_C_PP_2-pay at clinic is enabled from doctor side*/
		/*  Mention test case covered in all test below :RGB_5 ,VB_1-VB_24,Biiling_C_P_27-Biiling_C_P_33,Biiling_C_P_18,Biiling_C_P_23-Biiling_C_P_26
		 * Biiling_C_P_47-Biiling_C_P_48
		 */
	  
	  /*Test case:Billing_C_PP_17,Biiling_C_P_44*/
	
	@Test(priority=64,groups = {"G1"})
	public void consumerPcPaymentCashWalletEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Cash Wallet");
		String paymentCheck="Cash, Wallet"; 
		String paidStatus="Cash, Wallet";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageValidation();
		 bs.paymentCashWallet();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCashWalletPdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCashWalletOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentCashWallet();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCashWalletPdfValidation();
	 }
	@Test(priority=65,groups = {"G2"})
	public void consumerPcPaymentCashDebitCardEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Cash Debit Card");
		String paymentCheck="Cash, Debit card"; 
		String paidStatus="Cash, Debit card";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageValidation();
		 bs.paymentCashDebitCard();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCashCardPdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCashCardOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentCashDebitCard();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCashCardPdfValidation();
	 }
	@Test(priority=66,groups = {"G3"})
	public void consumerPcPaymentCashCreditCardEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Cash Credit Card");
		String paymentCheck="Cash, Credit card"; 
		String paidStatus="Cash, Credit card";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageValidation();
		 bs.paymentCashCreditCard();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCashCardPdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCashCardOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentCashCreditCard();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCashCardPdfValidation();
	 }
	@Test(priority=67,groups = {"G3"})
	public void consumerPcPaymentCashEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Cash");
		String paymentCheck="Cash"; 
		String paidStatus="Cash";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageValidation();
		 bs.paymentCash();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCashPdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCashOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentCash();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCashPdfValidation();
	 }
	@Test(priority=68,groups = {"G1"})
	public void consumerPcPaymentCreditCardEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Credit Card");
		String paymentCheck="Credit card"; 
		String paidStatus="Credit card";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageValidation();
		 bs.paymentCreditCard();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCardPdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCardOption();
	  bs.billingCreatePaymentOptionDisplay();
	  bs.paymentCreditCard();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCardPdfValidation();
	 }
	@Test(priority=69,groups = {"G2"})
	public void consumerPcPaymentWalletEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Wallet");
		String paymentCheck="Wallet"; 
		String paidStatus="Wallet";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageValidation();
		 bs.paymentWallet();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingWalletPdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedWalletOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentWallet();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingWalletPdfValidation();
	 }
	
	//No Service
	
	@Test(priority=70,groups = {"G2"})
	public void consumerPcPaymentCashWalletNoServiceEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Cash Wallet No Service");
		String paymentCheck="Cash, Wallet"; 
		String paidStatus="Cash, Wallet";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageNoServiceValidation();
		 bs.paymentCashWallet();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCashWalletNoServicePdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer. consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCashWalletOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentCashWallet();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCashWalletNoServicePdfValidation();
	 }
	@Test(priority=71,groups = {"G1"})
	public void consumerPcPaymentCashDebitCardNoServiceEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Cash Debit Card No Service");
		String paymentCheck="Cash, Debit card"; 
		String paidStatus="Cash, Debit card";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageNoServiceValidation();
		 bs.paymentCashDebitCard();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCashCardNoServicePdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCashCardOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentCashDebitCard();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCashCardNoServicePdfValidation();
	 }
	@Test(priority=72,groups = {"G1"})
	public void consumerPcPaymentCashNoServiceEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Cash No Service");
		String paymentCheck="Cash"; 
		String paidStatus="Cash";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageNoServiceValidation();
		 bs.paymentCash();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCashNoServicePdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCashOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentCash();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCashNoServicePdfValidation();
	 }
	@Test(priority=73,groups = {"G3"})
	public void consumerPcPaymentCreditCardNoServiceEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Credit Card No Service");
		String paymentCheck="Credit card"; 
		String paidStatus="Credit card";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageNoServiceValidation();
		 bs.paymentCreditCard();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingCardNoServicePdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedCardOption();
	  bs.billingCreatePaymentOptionDisplay();
	  bs.paymentCreditCard();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingCardNoServicePdfValidation();
	 }
	@Test(priority=74,groups = {"G2"})
	public void consumerPcPaymentWalletNoServiceEmrValidation()throws Exception
	{
		logger = Reports.extent.createTest("consumer Pay ata Clinic Payment Emr Validation with Wallet No Service");
		String paymentCheck="Wallet"; 
		String paidStatus="Wallet";
		consumer.consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		 appt.appointmentBookedStatus();
		 appt.appointmentPaymentModePcStatusBooked();
		 appt.bookedAppointmentOptionValidation();
		 appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		 consumer.consumerPcBillingPageNoServiceValidation();
		 bs.paymentWallet();
		 bs.billingCreateBillPopUpValidation();
		 bs.billingWalletNoServicePdfValidation();
		 Web_GeneralFunctions.wait(5);
	  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
	  Web_GeneralFunctions.wait(5);
	  appt.getAppointmentDropdown();
	  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  appt.appointmentPaymentModePcStatusPaid(paidStatus);
	  consumer.consumerViewBillAndCancelBillAppointmentValidation(paymentCheck);
	  bs.paymentRemoveSelectedWalletOption();
	  bs. billingCreatePaymentOptionDisplay();
	  bs.paymentWallet();
	  bs. billingCreateBillPopUpValidation();
	  bs.billingWalletNoServicePdfValidation();
	 }
	

}

