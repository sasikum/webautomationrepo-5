package com.tatahealth.EMR.Scripts.Login;

import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class Login_Doctor {

	public static ExtentTest logger;
	public static WebDriver driver;
	public static String executionName = "Default Name";
	

	public synchronized static void LoginTest() throws Exception {


		driver = Web_Testbase.start(executionName, driver);
		logger = Reports.extent.createTest("Login Doctor");

		String userName = SheetsAPI.getDataConfig(Web_Testbase.input + ".username");
		String password = SheetsAPI.getDataConfig(Web_Testbase.input + ".password");
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

		driver.get(URL);
		Web_GeneralFunctions.wait(3);

		LoginPage loginPage = new LoginPage();
		Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
				"Sending username to textbox", driver, logger);
		Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
				"Sending password to textbox", driver, logger);
		Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver, logger);
		Web_GeneralFunctions.wait(3);
		SweetAlertPage swaPage = new SweetAlertPage();
		if (swaPage.getSweetAlertCheck(driver, logger)) {
			Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert", driver,
					logger);
		}

		Web_GeneralFunctions.wait(15);
	}

	public synchronized static void LoginTestwithDiffrentUser(String enterUserName) throws Exception {

		driver = Web_Testbase.start(executionName, driver);
		logger = Reports.extent.createTest("Login Doctor");

		String userName = "";
		String password = "";
		String URL = "";
		LoginPage loginPage = new LoginPage();
		SweetAlertPage swaPage = new SweetAlertPage();

		String x = enterUserName; // change this to 1, true, false, hello world, or something that is not one of
									// those
		switch (x) { // when x is switched...
		case "StgDocforstaff": {
			userName = "StgDocforstaff";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

    		Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(3);
		}
			break;

		case "Doctor": {
			userName = "aut006";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
		}
			break;

		case "Digitizer": 

			userName = SheetsAPI.getDataConfig(Web_Testbase.input + ".aut003");
			password = SheetsAPI.getDataConfig(userName + ".password");
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;

		case "Staff_Paramedic": 
			userName = SheetsAPI.getDataConfig(Web_Testbase.input + ".aut005");
			password = SheetsAPI.getDataConfig(userName + ".password");
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

    		Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
		
			break;
		case "shinchan":
			userName = "shinchan";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "SonalinQAEMROne":
			userName = "SonalinQAEMROne";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "DontBooKApptsFromThisDoc":
			userName = "DontBooKApptsFromThisDoc";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Don":
			userName = "Don";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Kasu":
			userName = "Kasu";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Saket":
			userName = "Saket";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
				URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Topaa":
			userName = "Topaa";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;

		case "Baba":
			userName = "Baba";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}


			Web_GeneralFunctions.wait(15);
			break;
		case "NewCalCentre":
			userName = "NewCalCentre";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
			
		case "StaffRecep":
			userName = "StaffRecep";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			
			break;
			
			
		case "StaffParamedic":
			userName = "StaffParamedic";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
			
			
		case "DocDigitizer":
			userName = "DocDigitizer";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}
			Web_GeneralFunctions.wait(15);
			break;
			
		case "NEWDOCNOCALCENTRE":
			userName = "NEWDOCNOCALCENTRE";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}
			Web_GeneralFunctions.wait(15);
			break;
		case "AutomationStg":
			userName = "AutomationStg";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}
			Web_GeneralFunctions.wait(15);
			break;


		

		case "AutomationOne":
			//to test referral header
			userName = "AutomationOne";

			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}



			Web_GeneralFunctions.wait(15);
			break;
			
		case "Palle":
			userName = "Palle";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}


			Web_GeneralFunctions.wait(15);
			break;

			
		case "Lego":
			
			userName = "Lego";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}
			Web_GeneralFunctions.wait(15);
			break;

		}
	}
	public synchronized static void LoginTestwithoutNewInstance(String enterUserName) throws Exception {
		
		logger = Reports.extent.createTest("Login Doctor");

		String userName = "";
		String password = "";
		String URL = "";
		LoginPage loginPage = new LoginPage();
		SweetAlertPage swaPage = new SweetAlertPage();

		String x = enterUserName; // change this to 1, true, false, hello world, or something that is not one of
									// those
		switch (x) { // when x is switched...
		case "StgDocforstaff": {
			userName = "StgDocforstaff";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

    		Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(3);
		}
			break;

		case "Doctor": {
			userName = "aut006";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
		}
			break;

		case "Digitizer": 

			userName = SheetsAPI.getDataConfig(Web_Testbase.input + ".aut003");
			password = SheetsAPI.getDataConfig(userName + ".password");
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;

		case "Staff_Paramedic": 
			userName = SheetsAPI.getDataConfig(Web_Testbase.input + ".aut005");
			password = SheetsAPI.getDataConfig(userName + ".password");
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

    		Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
		
			break;
		case "shinchan":
			userName = "shinchan";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "SonalinQAEMROne":
			userName = "SonalinQAEMROne";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "DontBooKApptsFromThisDoc":
			userName = "DontBooKApptsFromThisDoc";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Don":
			userName = "Don";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Kasu":
			userName = "Kasu";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Saket":
			userName = "Saket";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
				URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
		case "Topaa":
			userName = "Topaa";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;

		case "Baba":
			userName = "Baba";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}


			Web_GeneralFunctions.wait(15);
			break;
		case "NewCalCentre":
			userName = "NewCalCentre";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
			
		case "StaffRecep":
			userName = "StaffRecep";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			
			break;
			
			
		case "StaffParamedic":
			userName = "StaffParamedic";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);

			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}

			Web_GeneralFunctions.wait(15);
			break;
			
			
		case "DocDigitizer":
			userName = "DocDigitizer";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}
			Web_GeneralFunctions.wait(15);
			break;
			
		case "NEWDOCNOCALCENTRE":
			userName = "NEWDOCNOCALCENTRE";
			password = SheetsAPI.getDataProperties(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}
			Web_GeneralFunctions.wait(15);
			break;
			
		case "AutomationStg":
			//to test referral header
			userName = "AutomationStg";
			password = SheetsAPI.getDataConfig(Web_Testbase.input + "." + userName);
			URL = SheetsAPI.getDataConfig(Web_Testbase.input + ".EMRURL");

			driver.get(URL);

			Web_GeneralFunctions.wait(3);

			Web_GeneralFunctions.sendkeys(loginPage.getUsernameTextBox(driver, logger), userName,
					"Sending username to textbox", driver, logger);
			Web_GeneralFunctions.sendkeys(loginPage.getPasswordTextBox(driver, logger), password,
					"Sending password to textbox", driver, logger);
			Web_GeneralFunctions.click(loginPage.getLoginBtn(driver, logger), "Clicking on Login Button", driver,
					logger);


			Web_GeneralFunctions.wait(3);
			if (swaPage.getSweetAlertCheck(driver, logger)) {
				Web_GeneralFunctions.click(swaPage.getYesBtn(driver, logger, 20), "Clicking on Yes in sweet Alert",
						driver, logger);
			}
			Web_GeneralFunctions.wait(15);
			break;
		}
	}
	public  static synchronized  void Logout(){
		LoginPage loginPage = new LoginPage();
		try {
			loginPage.getLogoutBtn(driver, logger).click();
			driver.manage().deleteAllCookies();
		} catch (Exception e) {
			driver.manage().deleteAllCookies();
		}
	}
	public  static synchronized  void LogoutTest(){
		LoginPage loginPage = new LoginPage();
		try {
			loginPage.getLogoutBtn(driver, logger).click();
			driver.manage().deleteAllCookies();
			driver.close();
			driver.quit();
		} catch (Exception e) {
			driver.manage().deleteAllCookies();
			driver.close();
			driver.quit();
		}
	}
	
}

